
import img from  '~/assets/img/usa.png';

export default () => {
  return new Promise(function (resolve) {
    resolve({
      welcome: 'Welcome',
      lang: {
        text: 'en',
        img: img,
      },
      HeaderCenter: {
        title: 'Uzbek Republican commodity exchange',
      },
    })
  });
}
