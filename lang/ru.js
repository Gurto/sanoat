
import img from  '~/assets/img/russia.png';

export default () => {
  return new Promise(function (resolve) {
    resolve({
      welcome: 'Welcome',
      lang: {
        text: 'ru',
        img: img,
      },
      HeaderCenter: {
        title: 'Узбекская республиканская товарно-сырьевая биржа',
      },
    })
  });
}