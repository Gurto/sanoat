
import img from  '~/assets/img/uzbekistan.png';

export default () => {
  return new Promise(function (resolve) {
    resolve({
      welcome: 'Welcome',
      lang: {
        text: 'uz',
        img: img,
      },
      HeaderCenter: {
        title: 'O\'zbekiston respublika tovar-xomashyo birjasi',
      },
    })
  });
}
