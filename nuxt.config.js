
export default {
  generate: {},
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon2.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    {src: '~/assets/scss/style.scss', lang: 'scss'},
    {src: '~/assets/scss/responsive.scss', lang: 'scss'},
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    ['nuxt-i18n', {
      defaultLocale: 'ru',
      seo: false,
      locales: [
        {
          code: 'ru',
          file: 'ru.js',
          iso: 'ru_RU',
          name: 'RU',
        },
        {
          code: 'uz',
          file: 'uz.js',
          iso: 'uz_UZ',
          name: 'UZ',
        },
        {
          code: 'en',
          file: 'en.js',
          iso: 'en_EN',
          name: 'EN',
        }
      ],
      lazy: true,
      langDir: 'lang/',
    }],
  ],

  axios: {
    credentials: true,
    proxy: true,
  },

  styleResources: {
    // your settings here
    scss: [
      '~/assets/scss/vars/*.scss',
    ],
  },

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    splitChunks: {
      layouts: true
    },
    extend(config, ctx) {
    }
  }
}
