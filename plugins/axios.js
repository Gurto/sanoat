export default function ({$axios, redirect, $route, app}) {
  $axios.onRequest((config) => {
    let token = app.$cookies.get('Auth');
    if (token) {
      config.headers.common['Authorization'] = `Bearer ${decodeURI(token)}`;
    }
    config.headers.common['front-header'] = true;
  });

  $axios.onError(({response}) => {
    if (process.server) {
      console.log(`[${response && response.status}] ${response && response.request.path}`);
      console.log(response && response.data);
    }
  });

  $axios.onResponseError(({response}) => {
    const code = parseInt(response && response.status);
    if (code === 401) {
      app.$cookies.remove('Auth');
      // if (process.client) {
      //   if (window.location.pathname !== '/login')
      //     window.location = `${window.location.origin}/login`;
      // }
    }

    if (code === 403) {

    }
  });
}
